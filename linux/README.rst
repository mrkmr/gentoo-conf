=====================
Gentoo Kernel Configs
=====================

- fyodor

  - description: Lenovo T470 with NVME drive.
  - version: 5.10.27-gentoo

- immanuel

  - description: Desktop AMD 2400G (APU) with an M.2 drive.
